from setuptools import setup

setup(
    name='ringmelib',
    version='0.0.1',
    packages=['ringmelib'],
    description="This is a library for ringme rest-api requests",
    python_requires='>=2.7',
)