Для установки необходимо выполнить команду:
pip install git+{адрес репозитория}#egg=ringmelib

Адрес репозитория можно взять из раздела git clone
Пример: pip install git+https://ititaev@bitbucket.org/ringme/ringmelib.git#egg=ringmelib

Также git+{адрес репозитория}#egg=ringmelib можно добавить в файл requirements.txt вашего проекта